import red from "chalk";
import yellow from "chalk";
import green from "chalk";

function stylizeText(txt: string, fn: Function) {
  console.log(fn(txt));
}

export function bad(txt: string) {
  stylizeText(txt, red);
}

export function warn(txt: string) {
  stylizeText(txt, yellow);
}

export function good(txt: string) {
  stylizeText(txt, green);
}
