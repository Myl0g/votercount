import * as https from "https";
import * as url from "url";
import * as fs from "fs";
import Poll from "./Poll";
import { bad } from "./logging";
import { PollAlreadyExistsError } from "./PollError";

https
  .createServer(
    {
      key: fs.readFileSync("../privkey.pem"), // private key used for SSL certification
      cert: fs.readFileSync("../fullchain.pem") // SSL certificate
    },
    (request, response) => {
      response.writeHead(200, {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept"
      });

      if (request.method === "GET") {
        console.log("New request received!");

        const parsed = url.parse(request.url, true).query;
        console.log(`- Type: ${parsed.get}`);

        if (parsed.get === "makePoll") {
          do {
            let poll: Poll;
            let name: string;
            let options: string[];

            try {
              name = String(parsed.name);
              options = JSON.parse(String(parsed.options));
            } catch {
              bad("Couldn't parse options to create poll!");
              response.end("Couldn't parse options to create poll!");
              break;
            }

            try {
              poll = new Poll(name, options);
            } catch(e) {
              if (e instanceof PollAlreadyExistsError) {
                bad("Poll with that name already exists!");
                response.end("Poll with that name already exists!");  
              } else {
                bad("Unknown error while creating poll");
                response.end("Poll with that name already exists!");
              }
              break;
            }

            poll.save();
          } while (false);
        }
      }
    }
  )
  .listen(443);
