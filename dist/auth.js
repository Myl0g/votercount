"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const googleapis_1 = require("googleapis");
const fs = __importStar(require("fs"));
function authorize() {
    const TOKEN_PATH = "token.json";
    return new Promise((resolve, reject) => {
        fs.readFile("credentials.json", (err, content) => {
            if (err) {
                reject(err);
                console.log("- - Failure opening creds file.");
            }
            console.log("- - Parsing creds file...");
            const credentials = JSON.parse(String(content));
            const { client_secret, client_id, redirect_uris } = credentials.installed;
            const oAuth2Client = new googleapis_1.google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
            console.log("- - Parsed.");
            fs.readFile(TOKEN_PATH, (err, token) => {
                if (err) {
                    console.log("- - Couldn't open stored token");
                    reject(err);
                }
                console.log("- - Parsing tokens...");
                oAuth2Client.setCredentials(JSON.parse(String(token)));
                console.log("- - Parsed.");
                resolve(oAuth2Client);
            });
        });
    });
}
exports.default = authorize;
