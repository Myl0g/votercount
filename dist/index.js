"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const https = __importStar(require("https"));
const url = __importStar(require("url"));
const fs = __importStar(require("fs"));
https
    .createServer({
    key: fs.readFileSync("../privkey.pem"),
    cert: fs.readFileSync("../fullchain.pem")
}, (request, response) => {
    response.writeHead(200, {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
    });
    if (request.method === "GET") {
        console.log("New request received!");
        const parsed = url.parse(request.url, true).query;
        console.log(`- Type: ${parsed.get}`);
        if (parsed.get === "whatever") {
        }
    }
})
    .listen(443);
